//
//  String.HTMLDecode.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 1/13/19.
//  Copyright © 2019 Get My Game, LLC. All rights reserved.
//

import Foundation

extension String {
    func htmlDecoded()->String {
        
        guard (self != "") else { return self }
        
        var newStr = self
        
        let entities = [
            "&amp;" : "&",
            "&amp; " : "& ",
            "&apos;" : "'",
            "&#39;" : "'",
            "&rsquo;" : "'",
            "&lt;p&gt;" : "",
            "&lt;/p&gt;" : "",
            "&mdash;" : "-",
            ]
        
        for (name,value) in entities {
            newStr = newStr.replacingOccurrences(of: name, with: value)
        }
        return newStr
    }
}
