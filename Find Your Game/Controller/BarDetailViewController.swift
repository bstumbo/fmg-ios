//
//  BarDetailViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 6/12/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit
import AEAccordion
import Cosmos
import Alamofire
import PopupDialog
import MapKit

protocol BarDetailDelegate {}

struct whatWatchData {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class BarDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var testTableViewData = [whatWatchData]()
    var currentUser = User()
    var delegate : BarDetailDelegate?
    var bar : Bar = Bar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        whatWatchTableView.delegate = self
        whatWatchTableView.dataSource = self
        testTableViewData = [whatWatchData(opened: false, title: "Teams", sectionData: [bar.teams]),
            whatWatchData(opened: false, title: "Leagues", sectionData: [bar.leagues]),
            whatWatchData(opened: false, title: "Sports", sectionData: [bar.sports])]
        barName.text = bar.title
        barDescription.text = bar.description.htmlDecoded()
        barDescription.contentOffset.y = 0
        barAddress.text = bar.location
        barHoursText.text = bar.hours.htmlDecoded()
        detailsBarImage.image = self.prepareBarImage(barImageURL: bar.imageURL)
        directionsButton.backgroundColor = UIColor(red:0.16, green:0.74, blue:0.72, alpha:1)
        directionsButton.addTarget(self, action: #selector(BarDetailViewController.directionsButtonAction(_:)), for: UIControl.Event.touchUpInside)
        mapButton.addTarget(self, action: #selector(BarDetailViewController.directionsButtonAction(_:)), for: UIControl.Event.touchUpInside)
        rateBarButton.contentVerticalAlignment = UIControl.ContentVerticalAlignment.top
        removeEmptyDescription()
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.setHidesBackButton(false, animated:true);
    }
    
    @IBOutlet weak var whatWatchTableView: UITableView!
    @IBOutlet weak var barAddress: UILabel!
    @IBOutlet weak var barName: UILabel!
    @IBOutlet weak var detailsBarImage: UIImageView!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var barDescription: UITextView!
    @IBOutlet weak var directionsButton: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var rateBarButton: UIButton!
    @IBOutlet weak var barHoursText: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBAction func websiteButtClicked(_ sender: Any) {
        guard let url = URL(string: bar.website) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func callButtonClicked(_ sender: Any) {
        if let url = URL(string: "tel://\(bar.phone)") {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func rateButtonAction(_ sender: Any) {
        showRate(bar: bar)
    }
    
    //Prepare URL link to Objective-C image
    func prepareBarImage(barImageURL: String) -> UIImage {
        let imageUrl = URL(string: barImageURL)!
        let imageData = try! Data(contentsOf: imageUrl)
        return UIImage(data: imageData) ?? UIImage(named: "letter-x_318-26692")!
    }
    
    //Functionality to remove an empty bar description
    func removeEmptyDescription() {

        if (bar.description == "") {
            self.barDescription.translatesAutoresizingMaskIntoConstraints = false
            self.barDescription.isScrollEnabled = false
            var frame = self.barDescription.frame
            frame.size.height = self.barDescription.contentSize.height
            self.barDescription.frame = frame
            self.descriptionLabel.text = ""
        }
    }
    
    //Directions button action
    @objc func directionsButtonAction(_ sender: UIButton!) {
        let bar = self.bar
        //Defining destination
        let regionDistance:CLLocationDistance = 1000;
        let coordinates = CLLocationCoordinate2DMake(bar.lat, bar.long)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = bar.title
        mapItem.openInMaps(launchOptions: options)
    }
    
    //View Tableview functionality
    func numberOfSections(in tableView: UITableView) -> Int {
        tableView.isScrollEnabled = false;
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return self.bar.whatWatch.count
        if testTableViewData[section].opened == true {
            return testTableViewData[section].sectionData.count + 1
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = UITableViewCell()
            cell.textLabel?.text = testTableViewData[indexPath.section].title
            cell.accessoryView = UIImageView(image: UIImage(named: "AccordionIcon"))
        
            return cell
        } else {
            let cell = UITableViewCell()
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.text = testTableViewData[indexPath.section].sectionData[indexPath.row - 1]
            return cell
        }
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if testTableViewData[indexPath.section].opened == true {
            testTableViewData[indexPath.section].opened = false
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
            tableView.cellForRow(at: indexPath)?.accessoryView?.transform = CGAffineTransform(scaleX: -1, y: 1)
        } else {
            testTableViewData[indexPath.section].opened = true
            let sections = IndexSet.init(integer: indexPath.section)
            tableView.reloadSections(sections, with: .none)
            tableView.cellForRow(at: indexPath)?.accessoryView?.transform = CGAffineTransform(scaleX: 1, y: -1)
        }
    }
    
    //Rate bar modal
    func showRate(bar: Bar) {
        let ratingVC = RatingViewController()
        ratingVC.barName = bar.title
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC,
        buttonAlignment: .horizontal,
        transitionStyle: .bounceDown,
        tapGestureDismissal: false)
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
        //            self.label.text = "You canceled the rating dialog"
        }
        // Create second button
        let buttonTwo = DefaultButton(title: "RATE", height: 60) {
        //            self.label.text = "You rated \(ratingVC.cosmosStarRating.rating) stars"
        self.rateBar(bar: bar, rating: ratingVC, completion: {})
        }
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    //Rate bar functionality
    func rateBar(bar: Bar, rating: RatingViewController, completion: @escaping () -> ()) {
        
        do {
            let RATE_URL = "http://54.149.226.203/bar-rate/rate"
            let params : [String : Any] = [
                "user": [
                    "name": self.currentUser.name,
                    "email": self.currentUser.email,
                    "uid": self.currentUser.uid,
                ],
                "entity_type_id": "node",
                "entity_id": bar.nid,
                "vote_type_id": "fivestar",
                "value": rating.cosmosStarRating.rating
            ]
            let valid = JSONSerialization.isValidJSONObject(params)
            print(valid)
            Alamofire.request(RATE_URL, method: .post, parameters: params, encoding: JSONEncoding.default).responseString { response in
                completion()
            }
        }
        catch{}
    }
}
