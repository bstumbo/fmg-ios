//
//  UserLocationViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 9/12/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class UserLocationViewController: UIViewController {
    let locationManager = CLLocationManager()
    var currentUser = User()
    let geocoder = CLGeocoder()
   
    @IBOutlet weak var userLocationSearchTable: UITableView!
    @IBOutlet weak var currentLocationLabel: UILabel!
    @IBOutlet weak var setLocationMap: MKMapView!
    @IBOutlet weak var useCurrentLocation: UIButton!
    
    @IBAction func useCurrentLocationPressed(_ sender: Any) {
        currentUser.userLat = locationManager.location?.coordinate.latitude ?? 38.8935128
        currentUser.userLong = locationManager.location?.coordinate.longitude ?? -77.1547739
        self.userLocationSearchTable.isHidden = true
        self.viewDidAppear(true)
    }
    
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userLocationSearchTable.isHidden = true
        searchCompleter.delegate = self
        useCurrentLocation.backgroundColor = UIColor(red:0.16, green:0.74, blue:0.72, alpha:1)
        if (((locationManager.location?.coordinate.latitude) != nil) && ((locationManager.location?.coordinate.longitude) != nil)) {
            currentUser.userLat = locationManager.location?.coordinate.latitude ?? 38.8935128
            currentUser.userLong = locationManager.location?.coordinate.longitude ?? -77.1547739
        }
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.setLocationMap.addGestureRecognizer(gesture)
        // remove left buttons (in case you added some)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationController?.navigationBar.topItem?.titleView = imageView 
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let userController = tabBarController as? UserCurrentLocationController
        userController?.currentUser.userLat = currentUser.userLat ?? 38.8935128
        userController?.currentUser.userLong = currentUser.userLong ?? -77.1547739
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // remove left buttons (in case you added some)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        _ = tabBarController as? UserCurrentLocationController
        self.setRegion(location: CLLocation(latitude: currentUser.userLat, longitude: currentUser.userLong))
        let location = CLLocation(latitude: currentUser.userLat, longitude: currentUser.userLong)
        geocoder.reverseGeocodeLocation(location, completionHandler: { placemarks, error in
            if error == nil {
                let placeMark = placemarks?.first
                let address =  ((placeMark?.name) != nil) ? (placeMark?.name)! : ""
                let city = ((placeMark?.locality) != nil) ? (placeMark?.locality)! : ""
                let state = ((placeMark?.administrativeArea) != nil) ? (placeMark?.administrativeArea)!: ""
                let zip = ((placeMark?.postalCode) != nil) ? (placeMark?.postalCode)! : ""
                self.currentLocationLabel.text = "Search Location: " + address + "," + city + " " + state + " " + zip
            }
        })
    
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        userLocationSearchTable.isHidden = true
        view.endEditing(true)
    }
    
    func setRegion(location: CLLocation) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.08, longitudeDelta: 0.08))
        setLocationMap.setRegion(region, animated: true)
        displayLocation()
    }
    
    func displayLocation() {
        let allAnnotations = setLocationMap.annotations
        setLocationMap.removeAnnotations(allAnnotations)
        let locationPinCoord = CLLocationCoordinate2D(latitude: currentUser.userLat, longitude: currentUser.userLong)
        let annotation = MKPointAnnotation()
        annotation.coordinate = locationPinCoord
        setLocationMap.addAnnotation(annotation)
        setLocationMap.showAnnotations([annotation], animated: true)
    }

}

extension UserLocationViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        userLocationSearchTable.isHidden = false
        searchCompleter.queryFragment = searchText
    }
}

extension UserLocationViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        return cell
    }
}

extension UserLocationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let completion = searchResults[indexPath.row]
        
        let searchRequest = MKLocalSearch.Request(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        search.start { (response, error) in
            let coordinate = response?.mapItems[0].placemark.coordinate
            self.currentUser.userLat = coordinate?.latitude ?? 38.8950445
            self.currentUser.userLong = coordinate?.longitude ?? -77.1547739
            self.userLocationSearchTable.isHidden = true
            self.view.endEditing(true)
            self.viewDidAppear(true)
        }
    }
}

extension UserLocationViewController: MKLocalSearchCompleterDelegate {
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        //get result, transform it to our needs and fill our dataSource
        searchResults = completer.results
        userLocationSearchTable.reloadData()
    }

    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        //handle the error
        print(error.localizedDescription)
    }
}

