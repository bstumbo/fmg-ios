//
//  TableViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 5/10/18.
//  Copyright © Get My Game, LLC. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation
import Alamofire
import Firebase
import MapKit

//Write the protocol declaration here:
protocol SearchDelegate {}

class SearchTableViewController: UITableViewController, UISearchResultsUpdating, SearchResultDelegate {

    var delegate : SearchDelegate?
    var barSearchResult : Array<Bar> = []
    var searchTermResult : Array<Any> = []
    var location = CLLocation()
    var locationManager = CLLocationManager()
    var searchType = ""
    var currentUser = User()
    let db = Firestore.firestore()
    let geocoder = CLGeocoder()
    let FMG_URL = "http://54.149.226.203/bar-search/json"
    let teamsUrl = "http://54.149.226.203/teams/json"
    let leaguesUrl = "http://54.149.226.203/leagues/json"
    let sportsUrl = "http://54.149.226.203/sports/json"
    let timestamp = NSDate().timeIntervalSince1970
    var filteredArray = [String]()
    var address = ""
    var city = ""
    var state = ""
    var zipcode = ""
    var searchController = UISearchController()
    var resultController = UITableViewController()
    lazy var array : [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchController = UISearchController(searchResultsController: resultController)
        tableView.tableHeaderView = self.searchController.searchBar
        self.searchController.searchResultsUpdater = self
        self.resultController.tableView.delegate = self
        self.resultController.tableView.dataSource = self
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        geoCodeLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.setHidesBackButton(false, animated:true);
    }

    func updateSearchResults(for searchController: UISearchController) {
        self.filteredArray = array.filter({ (array: String) -> Bool in
            
            if array.contains(searchController.searchBar.text!) {
                return true
            }
            else {
                return false
            }
        })
        
        self.resultController.tableView.reloadData()
    }
    
    //Geocode search location
    func geoCodeLocation() {
        let searchLocation = CLLocation(latitude: self.currentUser.userLat, longitude: self.currentUser.userLong)
        geocoder.reverseGeocodeLocation(searchLocation, completionHandler: { placemarks, error in
            if error == nil {
                let placeMark = placemarks?.first
                self.address = (placeMark?.name)!
                self.city = (placeMark?.locality)!
                self.state = (placeMark?.administrativeArea)!
                self.zipcode = (placeMark?.postalCode)!
            }
        })
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        if tableView == resultController.tableView {
            return self.filteredArray.count
        }
        else {
            return self.array.count
        }

    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        
        if tableView == resultController.tableView {
            cell.textLabel?.text = self.filteredArray[indexPath.row]
        }
        else {
            cell.textLabel?.text = self.array[indexPath.row]
        }
        
        return cell
    }
    
    //MARK: - TableView Delegate Methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath ?? IndexPath(index: 0)) as! UITableViewCell
        let query = currentCell.textLabel?.text
        let params = buildParams(query: query!)
        //Record search in Firebase
        self.db.collection("users").document(self.currentUser.uid).collection("searches").document(String(timestamp)).setData([
            "timestamp": timestamp,
            "latitude": self.currentUser.userLat,
            "longitude": self.currentUser.userLong,
            "searchTerm": query ?? "",
            "searchType": self.searchType,
            "address": self.address,
            "city": self.city,
            "state": self.state,
            "zipcode": self.zipcode
        ]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added with ID:")
            }
        }
        
        //Make Query to FMG database
        self.getBarSearchData(url: FMG_URL, parameters: params, completion: {
           self.performSegue(withIdentifier: "goToResults", sender: self)
        })
        
        if tableView == resultController.tableView {
            self.resultController.dismiss(animated: false, completion: nil)
        }

    }
    
    //MARK: - Networking
    /***************************************************************/
    
    //Write the getBarData method here:
    func getBarSearchData(url: String, parameters: [String: String], completion: @escaping () -> ())  {
        Alamofire.request(url, method: .get, parameters: parameters).responseJSON { response in
            self.barSearchResult = []
    
           if response.result.isSuccess {
            let bars = response.result.value as! [[String : String]]
                for bar in bars {
                    let newBar = Bar()
                    newBar.title = bar["title"]!
                    newBar.nid = bar["nid"]!
                    newBar.lat = Double((bar["field_point"]! as NSString).doubleValue)
                    newBar.long = Double((bar["field_point_1"]! as NSString).doubleValue)
                    newBar.location = bar["field_location"]!
                    newBar.hours = bar["field_bar_hours"]!
                    newBar.description = bar["field_bar_description"]!
                    newBar.imageURL = bar["field_bar_image"]!
                    newBar.phone = bar["field_phone_number"]!
                    newBar.teams = bar["field_teams"]!
                    newBar.leagues = bar["field_leagues"]!
                    newBar.sports = bar["field_sports"]!
                    newBar.whatWatch = [newBar.teams, newBar.leagues, newBar.sports]
                    newBar.rating = Double((bar["value"]! as NSString).doubleValue)
                    newBar.website = bar["field_website"]!
                    let distance = self.location.distance(from: CLLocation(latitude: newBar.lat, longitude: newBar.long))/1609.344
                    newBar.distance = Double(round(100*distance)/100)
                    self.barSearchResult.append(newBar)
                }
           }
            else {
                print("Error \(String(describing: response.result.error))")

            }
            
            completion()
        }
    
    }
    
    func buildParams(query: String) -> [String : String] {
        var team =  ""
        var league = ""
        var sport = ""
        
        switch (searchType) {
        case "teams":
            team = query
        case "leagues":
            league = query
        case "sports":
            sport = query
        default:
            team = ""
        }
    
        let params : [String : String] = [
            "field_point_proximity-lat" : String(format: "%.2f", location.coordinate.latitude),
            "field_point_proximity-lng" : String(format: "%.2f", location.coordinate.longitude),
            "field_point_proximity" : "10",
            "tid" : team,
            "tid_1" : league,
            "tid_2" : sport,
            "_format" : "json"
        ]

        return params
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "goToResults" {
            let destinationVC = segue.destination as! SearchResultViewController
            destinationVC.delegate = self
            destinationVC.result = self.barSearchResult
            destinationVC.location = self.location
            destinationVC.currentUser = self.currentUser
            destinationVC.locationManager = self.locationManager
        }

    }
}
