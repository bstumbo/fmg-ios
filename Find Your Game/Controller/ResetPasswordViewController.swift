//
//  ResetPasswordViewController.swift
//  Get My Game
//
//  Created by Brandon on 11.07.18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var emailAccount: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailAccount.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func endEditing() {
        view.endEditing(true)
    }
}

extension ResetPasswordViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}

