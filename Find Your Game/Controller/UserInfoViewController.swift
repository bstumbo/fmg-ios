//
//  UserInfoViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 9/12/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit
import Firebase
import PopupDialog

class UserInfoViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    var currentUser = User()
    var ages = Array(10...100)
    var agePicker = UIPickerView()
    let db = Firestore.firestore()
    
    @IBOutlet var UserInfoView: UIView!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var userNameText: UITextField!
    @IBOutlet weak var ageText: UITextField!
    @IBOutlet weak var emailAddressText: UITextField!
    
    @IBOutlet weak var zipcodeText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        agePicker.delegate = self
        agePicker.dataSource = self
        ageText.inputView = agePicker
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.UserInfoView.addGestureRecognizer(gesture)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationController?.navigationBar.topItem?.titleView = imageView
        
        let nameLabel = UILabel()
        nameLabel.text = "Name"
        nameLabel.textColor = UIColor.black
        addLeftLabelTo(txtField: userNameText, andLabel: nameLabel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let userController = tabBarController as! UserCurrentLocationController
        self.currentUser = userController.currentUser
        self.userNameText.text = self.currentUser.name
        self.emailAddressText.text = self.currentUser.email
        self.ageText.text = String(self.currentUser.age)
        self.zipcodeText.text = String(self.currentUser.zipcode)
    }
    
    @IBAction func updateButtonClicked(_ sender: Any) {
        if((self.userNameText.text != "") && (self.emailAddressText.text != "") && (self.ageText.text != "")) {
            let userRecord = db.collection("users").document(self.currentUser.uid)
        
            // Set the updated user fields
            userRecord.updateData([
                "Username": self.userNameText.text ?? "",
                "Email": self.emailAddressText.text ?? "",
                "Age": self.ageText.text ?? "",
                "Zipcode": self.zipcodeText.text ?? "",
            ]) { err in
                if let err = err {
                    let title = "Foul!"
                    let message = "Something went wrong. Please try your update again"
                    let popup = PopupDialog(title: title, message: message)
                    let OK = CancelButton(title: "OK") {}
                    popup.addButtons([OK])
                    self.present(popup, animated: true, completion: nil)
                } else {
                    print("Document successfully updated")
                    let title = "Victory!"
                    let message = "Your profile has been updated."
                    let popup = PopupDialog(title: title, message: message)
                    let OK = CancelButton(title: "OK") {}
                    popup.addButtons([OK])
                    self.present(popup, animated: true, completion: nil)
                }
            }
        }
        else {
            //Pop modal to tell user to fill out all fields
            self.missingFieldsError()
        }
       
    }
    
    @IBAction func resetPasswordButtonClicked(_ sender: Any) {        
        self.sendResetEmail(email: self.currentUser.email)
        let title = "Email Sent"
        let message = "A password reset email has been sent to your registered email address"
        let popup = PopupDialog(title: title, message: message)
        let OK = CancelButton(title: "OK") {}
        popup.addButtons([OK])
        self.present(popup, animated: true, completion: nil)
    }
    
    @IBAction func logoutButtonClicked(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "welcome1")
            self.navigationController?.pushViewController(newViewController, animated: true)
        }
        catch let err{
           print(err)
        }
        
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) { 
        view.endEditing(true)
    }
    
    func sendResetEmail(email: String) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            // ...
        }
    }
    
    func addLeftLabelTo(txtField: UITextField, andLabel label: UILabel) {
        var leftLabelView : UILabel = UILabel()
        leftLabelView = label
        txtField.leftView = leftLabelView
        txtField.leftViewMode = .always
    }
    
    func missingFieldsError() {
        let title = "Error"
        let errorMessage = "Please fill out all fields"
        let popup = PopupDialog(title: title, message: errorMessage)
        
        let OK = CancelButton(title: "OK") {}
        
        popup.addButtons([OK])
        self.present(popup, animated: true, completion: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(ages[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        ageText.text = String(ages[row])
        self.view.endEditing(false)
    }
    
}
