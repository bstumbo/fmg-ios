//
//  SearchResultTableViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 6/6/18.
//  Copyright © Get My Game, LLC. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MapKit
import PopupDialog
import Cosmos
import Foundation

protocol SearchResultDelegate  {}

class specialAnnotation: MKPointAnnotation {
    var number : Int = 0;
}

class BarTableViewCell: UITableViewCell {
    @IBOutlet weak var distanceToBar: UILabel!
    @IBOutlet weak var barName: UILabel!
    @IBOutlet weak var barImage: UIImageView!
    @IBOutlet weak var directionsButton: UIButton!
    @IBOutlet weak var rate: UIButton!
    @IBOutlet weak var ratingView: CosmosView!
}

class SearchResultViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var locationManager = CLLocationManager()
    var delegate : SearchResultDelegate?
    var result : Array<Bar>? = []
    var location = CLLocation()
    var currentUser = User()
    
    @IBOutlet weak var searchResultTableView: UITableView!
    @IBOutlet weak var searchResultsMap: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        result?.sort{ $0.distance < $1.distance }
        searchResultsMap.delegate = self
        searchResultTableView.delegate = self
        searchResultTableView.dataSource = self
        getLocations()
        setRegion(location: location)
        
        if((result?.count)! < 1) {
            noResults()
        }
        
        checkLocation()
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.setHidesBackButton(false, animated:true);
        self.searchResultTableView.estimatedRowHeight = 210
        self.searchResultTableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        let selection = self.searchResultTableView.indexPathForSelectedRow
        
        if ((selection) != nil) {
            self.searchResultTableView.deselectRow(at: selection ?? NSIndexPath(row: 0, section: 0) as IndexPath, animated: true)
        }
        
    }
    
    //Rate Bar Modal
    func showRate(bar: Bar) {
        let ratingVC = RatingViewController()
        ratingVC.barName = bar.title
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: false)
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
//            self.label.text = "You canceled the rating dialog"
        }
        // Create second button
        let buttonTwo = DefaultButton(title: "RATE", height: 60) {
            self.rateBar(bar: bar, rating: ratingVC, completion: {})
        }
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    
    //Set region for the results map
    func setRegion(location: CLLocation) {
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude), span: MKCoordinateSpan(latitudeDelta: 0.08, longitudeDelta: 0.08))
        searchResultsMap.setRegion(region, animated: true)
        
    }
    
    //Get locations of all bars and current user location
    func getLocations() {
        locationManager.startUpdatingLocation()
        var tag : Int = 0
        
        for bar in result! {
            self.displayLocation(barLocation: CLLocation(latitude: bar.lat, longitude: bar.long), bar: bar, tag: tag )
            tag += 1
        }
        
        let locationPinCoord = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let annotation = MKPointAnnotation()
        annotation.coordinate = locationPinCoord
        annotation.title = "Me"
        searchResultsMap.addAnnotation(annotation)
        searchResultsMap.showAnnotations([annotation], animated: true)
        }
    
    //Display bar locations
    func displayLocation(barLocation:CLLocation, bar: Bar, tag: Int) {
        let scale = MKScaleView(mapView: searchResultsMap)
        scale.scaleVisibility = .visible // always visible
        searchResultsMap.addSubview(scale)
        let locationPinCoord = CLLocationCoordinate2D(latitude: barLocation.coordinate.latitude, longitude: barLocation.coordinate.longitude)
        let annotation = specialAnnotation()
        annotation.coordinate = locationPinCoord
        annotation.title = bar.title
        annotation.number = tag
        searchResultsMap.addAnnotation(annotation)
        searchResultsMap.showAnnotations([annotation], animated: true)
    }
    
    //View tableview utlity functionality
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.result!.count
    }

   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as! BarTableViewCell
    cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        if let bar = self.result?[indexPath.row] {
            cell.tag = indexPath.row
            cell.directionsButton.tag = indexPath.row
            cell.rate.tag = indexPath.row
            cell.directionsButton.addTarget(self, action: #selector(SearchResultViewController.directionsButtonAction(_:)), for: UIControl.Event.touchUpInside)
            cell.rate.addTarget(self, action: #selector(SearchResultViewController.rateButtonAction(_:)), for: UIControl.Event.touchUpInside)
            cell.backgroundColor = UIColor.darkGray
            cell.barName?.text = bar.title
            cell.distanceToBar?.text = String(format:"%.1f", bar.distance) + " miles"
            cell.barImage.image = self.prepareBarImage(barImageURL: bar.imageURL)
            cell.directionsButton.backgroundColor = UIColor(red:0.16, green:0.74, blue:0.72, alpha:0.7)
            cell.rate.backgroundColor = UIColor(red:0.16, green:0.74, blue:0.72, alpha:0.7)
            cell.ratingView.rating = bar.rating
            cell.ratingView.settings.updateOnTouch = false
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToBarDetail", sender: self)
        
    }
    
    //Logic to prepare image URL to Objective-C image
    func prepareBarImage(barImageURL: String) -> UIImage {
        let imageUrl = URL(string: barImageURL)!
        let imageData = try! Data(contentsOf: imageUrl)
        
        return UIImage(data: imageData) ?? UIImage(named: "letter-x_318-26692")!
    }
    
    //Rate bar functionality
    func rateBar(bar: Bar, rating: RatingViewController, completion: @escaping () -> ()) {
        
        do {
            let RATE_URL = "http://54.149.226.203/bar-rate/rate"
            let params : [String : Any] = [
                "user": [
                    "name": self.currentUser.name,
                    "email": self.currentUser.email,
                    "uid": self.currentUser.uid,
                ],
                "entity_type_id": "node",
                "entity_id": bar.nid,
                "vote_type_id": "fivestar",
                "value": rating.cosmosStarRating.rating
            ]
            let valid = JSONSerialization.isValidJSONObject(params)
            print(valid)
            Alamofire.request(RATE_URL, method: .post, parameters: params, encoding: JSONEncoding.default).responseString { response in
                completion()
            }
        }
        catch{
            print("Rating wasn't able to be completed")
        }
    }
    
    //Rate button action
    @objc func rateButtonAction(_ sender: UIButton!) {
        let bar = self.result![sender.tag]
        showRate(bar: bar)
    }
    
    //Direction button action
    @objc func directionsButtonAction(_ sender: UIButton!) {
        let bar = self.result![sender.tag]
        //Defining destination
        let regionDistance:CLLocationDistance = 1000;
        let coordinates = CLLocationCoordinate2DMake(bar.lat, bar.long)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        let placemark = MKPlacemark(coordinate: coordinates)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = bar.title
        mapItem.openInMaps(launchOptions: options)
    }
    
    //Check current user location to see if in range of DC
    func checkLocation() {
        let dcLocation = CLLocation(latitude: 38.9072, longitude:-77.0369)
        let userLocation = CLLocation(latitude: (self.currentUser.userLat), longitude:(self.currentUser.userLong))
        let distance = (userLocation.distance(from: dcLocation) / 1000) * 0.62137
        
        if (distance > 25) {
            outOfRange()
        }
        
    }
    
    //No Results Modal if no search results
    func noResults() {
        let title = "No Results Found"
        let message = "There aren't any results for your search in this area. Try searching another team or league to find your game."
        let popup = PopupDialog(title: title, message: message)
        let noThanks = CancelButton(title: "No, Thanks") {}
        let OK = DefaultButton(title: "OK") {      
            _ = self.navigationController?.popViewController(animated: true)
    
        }
        popup.addButtons([OK, noThanks])
        self.present(popup, animated: true, completion: nil)
    }
    
    //Out of Range modal
    func outOfRange() {
        let title = "Out of Range"
        let message = "GetMyGame search results are currently only available in the Washington, DC area. Set your location to a DC address to find your game!"
        let popup = PopupDialog(title: title, message: message)
        let noThanks = CancelButton(title: "No, Thanks") {}
        let OK = DefaultButton(title: "OK") {
            self.tabBarController?.selectedIndex = 1
        }
        popup.addButtons([OK, noThanks])
        self.present(popup, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(sender!)
        let destinationVC = segue.destination as! BarDetailViewController
        
        if let indexPath = searchResultTableView.indexPathForSelectedRow {
            destinationVC.bar = (self.result?[indexPath.row])!
            destinationVC.currentUser = self.currentUser
        }
    }
}

//Extension for SearchResultViewController
extension SearchResultViewController: MKMapViewDelegate {
   //Map-related ultility functionality
   func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let rowIndex = view.annotation as! specialAnnotation
        self.searchResultTableView.scrollToRow(at: IndexPath(row: rowIndex.number, section: 0), at: .top, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView")
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
        }
        
        if let title = annotation.title, title == "Me" {
            annotationView?.image = UIImage(named: "selected_location")
        } else {
            annotationView?.image = UIImage(named: "small-bar-marker")
        }

        annotationView?.canShowCallout = true
        return annotationView
    }
}
