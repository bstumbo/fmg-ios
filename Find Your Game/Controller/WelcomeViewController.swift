//
//  WelcomeViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 10/17/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit
import FirebaseAuth
import CoreLocation

class WelcomeViewController: UIViewController {
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        Auth.auth().addStateDidChangeListener { auth, user in
            if user != nil {
                let logo = UIImage(named: "gmg-banner")
                let imageView = UIImageView(image:logo)
                self.navigationController?.isNavigationBarHidden = false
                self.navigationController?.navigationBar.topItem?.hidesBackButton = true
                self.navigationItem.titleView = imageView
                self.performSegue(withIdentifier: "alreadyLoggedIn", sender: (Any).self)// User is signed in.
            } else {
                // No user is signed in.
            }
        }
        super.viewDidLoad()
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = false
        self.navigationItem.titleView = imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

//Extension for ViewController
extension WelcomeViewController: CLLocationManagerDelegate {
    //Location manager utility functions
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lat = locations.last?.coordinate.latitude, let long = locations.last?.coordinate.longitude {
            print("\(lat),\(long)")
        } else {
            print("No coordinates")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func lookUpCurrentLocation(completionHandler: @escaping (CLPlacemark?) -> Void ) {
        // Use the last reported location.
        if let lastLocation = self.locationManager.location {
            let geocoder = CLGeocoder()
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(lastLocation, completionHandler: { (placemarks, error) in
                if error == nil {
                    let firstLocation = placemarks?[0]
                    completionHandler(firstLocation)
                }
                else {
                    // An error occurred during geocoding.
                    completionHandler(nil)
                }
            })
        }
        else {
            // No location was available.
            completionHandler(nil)
        }
    }
}
