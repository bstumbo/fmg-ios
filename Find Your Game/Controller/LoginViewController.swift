//
// LoginViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 10/17/18.
//  Copyright © 2018 Get My Game. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import PopupDialog
import Cosmos

class LoginGMGViewController: UIViewController {
    let db = Firestore.firestore()
    
    @IBOutlet var LoginView: UIView!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.LoginView.addGestureRecognizer(gesture)
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = false
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        self.forgotPassword()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        Auth.auth().signIn(withEmail: email.text!, password: password.text!) { (user, error) in
            if error != nil {
                self.wrongPassword()
            } else {
                Auth.auth().addStateDidChangeListener { auth, user in
                    if user != nil {
                        let logo = UIImage(named: "gmg-banner")
                        let imageView = UIImageView(image:logo)
                        self.navigationController?.isNavigationBarHidden = false
                        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
                        self.navigationItem.titleView = imageView
                        self.performSegue(withIdentifier: "loginNew", sender: (Any).self)
                    }
                }
            }
        }
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "login" {
//            Auth.auth().addStateDidChangeListener { auth, user in
//                if user != nil {
//                    let logo = UIImage(named: "gmg-banner")
//                    let imageView = UIImageView(image:logo)
//                    self.navigationController?.isNavigationBarHidden = false
//                    self.navigationController?.navigationBar.topItem?.hidesBackButton = true
//                    self.navigationItem.titleView = imageView
//                } else {
//                    // No user is signed in.
//                }
//            }
//        } else {
//            print("Document does not exist")
//        }
//    }
    
  
    @objc func checkAction(sender : UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func forgotPassword() {
        let forgotPasswordVC = ResetPasswordViewController()
        let popup = PopupDialog(viewController:forgotPasswordVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: false)
        
        let cancel = CancelButton(title: "Close") {}
        let submit = DefaultButton(title: "Submit") {
            self.sendResetEmail(email: forgotPasswordVC.emailAccount.text!)
        }
        popup.addButtons([cancel, submit])
        present(popup, animated: true, completion: nil)
    }
    
    func wrongPassword() {
        let title = "Wrong Username/Password"
        let message = "We don't recogninze that email address and password combination. Please try again."
        let popup = PopupDialog(title: title, message: message)
        let OK = CancelButton(title: "OK") {}
        popup.addButtons([OK])
        self.present(popup, animated: true, completion: nil)
    }
    
    func sendResetEmail(email: String) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            // ...
        }
    }
    
}



