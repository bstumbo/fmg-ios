//
//  TermsandConditionsViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 1/21/19.
//  Copyright © 2019 Get My Game, LLC. All rights reserved.
//

import UIKit

class TermsandConditionsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        navigationItem.setHidesBackButton(false, animated:true);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
