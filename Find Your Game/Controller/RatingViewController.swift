//
//  RatingViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 11.07.16.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit
import Cosmos

class RatingViewController: UIViewController {
    var barName : String = ""
    
    @IBOutlet weak var cosmosStarRating: CosmosView!

    @IBOutlet weak var barLabel: UILabel!
    @IBOutlet weak var customText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        barLabel.text = barName
        customText.text = "How do you rate your sporting experience at " + barName + "?"
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func endEditing() {
        view.endEditing(true)
    }
    
}

extension RatingViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        endEditing()
        return true
    }
}

