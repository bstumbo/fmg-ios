//
//  FMGViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 4/23/18.
//  Copyright © Get My Game, LLC. All rights reserved.
//

import UIKit
import CoreLocation
import PopupDialog
import Firebase
import Alamofire

class Global {
    var searchType = ""
}

class GMGViewController: UIViewController, SearchDelegate, UINavigationControllerDelegate {
    
    let global = Global()
    let locationManager = CLLocationManager()
    var currentUser = User()
    let db = Firestore.firestore()
    let teamsUrl = "http://54.149.226.203/teams/json"
    let leaguesUrl = "http://54.149.226.203/leagues/json"
    let sportsUrl = "http://54.149.226.203/sports/json"
    var searchTermResult : Array<Any> = []
    var taxonomyNames : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationController?.navigationBar.topItem?.titleView = imageView
        locationManager.delegate = self as! CLLocationManagerDelegate
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        teamsButton.backgroundColor = UIColor(red:0.16, green:0.74, blue:0.72, alpha:1)
        leaguesButton.backgroundColor = UIColor(red:0.16, green:0.74, blue:0.72, alpha:1)
        sportsButton.backgroundColor = UIColor(red:0.16, green:0.74, blue:0.72, alpha:1)
        //Setup currentUser
        if (currentUser.uid == "") {
            let userRef = db.collection("users").document((Auth.auth().currentUser?.uid)!)
            userRef.getDocument { (document, error) in
                if let document = document, document.exists {
                    let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                    self.currentUser.uid = (Auth.auth().currentUser?.uid)!
                    self.currentUser.name = document.data()?["Username"] as! String
                    self.currentUser.email = document.data()?["Email"] as! String
                    self.currentUser.age = Int((document.data()?["Age"] as! NSString).floatValue)
                    self.currentUser.zipcode = document.data()?["Zipcode"] as! String
                } else {
                    print("Document does not exist")
                }
            }
        }
        checkLocation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationController?.navigationBar.topItem?.titleView = imageView
        let userController = tabBarController as? UserCurrentLocationController
        currentUser.userLat = userController?.currentUser.userLat ?? 38.8935128
        currentUser.userLong = userController?.currentUser.userLong ?? -77.1546602
        checkLocation()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = true
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationController?.navigationBar.topItem?.titleView = imageView
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return !(identifier == "login")
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if (currentUser.userLat == 0 || currentUser.userLong == 0) {
            let userController = tabBarController as? UserCurrentLocationController
            userController?.currentUser.userLat = locationManager.location?.coordinate.latitude ?? 38.8935128
            userController?.currentUser.userLong = locationManager.location?.coordinate.longitude ?? -77.1546602
            userController?.currentUser.uid = self.currentUser.uid
            userController?.currentUser.name = self.currentUser.name
            userController?.currentUser.email = self.currentUser.email
            userController?.currentUser.age = self.currentUser.age
            userController?.currentUser.zipcode = self.currentUser.zipcode
        }
    
    }
    
    //Check users current location to see if out of range of DC
    func checkLocation() {
        let dcLocation = CLLocation(latitude: 38.9072, longitude:-77.0369)
        let userLocation = CLLocation(latitude: (self.currentUser.userLat), longitude:(self.currentUser.userLong))
        let distance = (userLocation.distance(from: dcLocation) / 1000) * 0.62137
        
        if (distance > 25) {
            outOfRange()
        }
        
    }
    
    //Out of Range modal
    func outOfRange() {
        let title = "Out of Range"
        let message = "GetMyGame search results are currently only available in the Washington, DC area. Set your location to a DC address to find your game!"
        let popup = PopupDialog(title: title, message: message)
        let noThanks = CancelButton(title: "No, Thanks") {}
        let OK = DefaultButton(title: "OK") {
            self.tabBarController?.selectedIndex = 1
        }
        popup.addButtons([OK, noThanks])
        self.present(popup, animated: true, completion: nil)
    }
    
    //Get search taxonomy terms here
    func getSearchTermsData(url: String, completion: @escaping () -> ()) {
        let params : [String : String] = ["_format" : "json"]
        Alamofire.request(url, method: .get, parameters: params).responseJSON { response in
            self.searchTermResult = []
            
            if response.result.isSuccess {
                let terms = response.result.value as! [[String : String]]
                for term in terms {
                    let name = term["name"]
                    self.searchTermResult.append(name ?? "name")
                }
            }
            else {
                print("Error \(String(describing: response.result.error))")
            }
            completion()
        }
        
    }
    
    //MARK - Search
    @IBAction func teamSearchPressed(_ sender: UIButton) {
        self.getSearchTermsData(url: teamsUrl, completion: {
            self.global.searchType = "teams"
            for item in self.searchTermResult {
                self.taxonomyNames.append(item as! String)
            }
            self.performSegue(withIdentifier: "goToSearch", sender: self)
        })
    }
    @IBAction func leagueSearchPressed(_ sender: UIButton) {
        self.getSearchTermsData(url: leaguesUrl, completion: {
            self.global.searchType = "leagues"
            for item in self.searchTermResult {
                self.taxonomyNames.append(item as! String)
            }
            self.performSegue(withIdentifier: "goToSearch", sender: self)
        })
    }
    @IBAction func sportsSearchPressed(_ sender: UIButton) {
        self.getSearchTermsData(url: sportsUrl, completion: {
            self.global.searchType = "sports"
            for item in self.searchTermResult {
                self.taxonomyNames.append(item as! String)
            }
            self.performSegue(withIdentifier: "goToSearch", sender: self)
        })
    }
    @IBOutlet weak var teamsButton: UIButton!
    @IBOutlet weak var leaguesButton: UIButton!
    @IBOutlet weak var sportsButton: UIButton!
    
    //Segue
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
        if segue.identifier == "goToSearch" {
        
            let destinationVC = segue.destination as! SearchTableViewController
            destinationVC.delegate = self
            destinationVC.searchType = self.global.searchType
            destinationVC.array = self.taxonomyNames
            destinationVC.locationManager = self.locationManager
                destinationVC.location = CLLocation(latitude: self.locationManager.location?.coordinate.latitude ?? 38.8935128, longitude:self.locationManager.location?.coordinate.longitude ?? -77.1546602)
            if(self.currentUser.userLat == 0.0 && self.currentUser.userLong == 0.0) {
                destinationVC.location = CLLocation(latitude: self.locationManager.location?.coordinate.latitude ?? 38.8935128, longitude: self.locationManager.location?.coordinate.longitude ?? -77.1546602)
                self.currentUser.userLat = self.locationManager.location?.coordinate.latitude ?? 38.8935128
                self.currentUser.userLong = self.locationManager.location?.coordinate.longitude ?? -77.1546602
            }
            else {
                destinationVC.location = CLLocation(latitude: self.currentUser.userLat, longitude: self.currentUser.userLong)
            }
            
            destinationVC.currentUser = self.currentUser
            self.searchTermResult = []
            self.taxonomyNames = []
        }
    }
}

//Extension for FMGViewController
extension GMGViewController: CLLocationManagerDelegate {
    //Location manager utility functions
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let lat = locations.last?.coordinate.latitude, let long = locations.last?.coordinate.longitude {
            print("\(lat),\(long)")
        } else {
            print("No coordinates")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    func lookUpCurrentLocation(completionHandler: @escaping (CLPlacemark?) -> Void ) {
        // Use the last reported location.
        if let lastLocation = self.locationManager.location {
            let geocoder = CLGeocoder()
            // Look up the location and pass it to the completion handler
            geocoder.reverseGeocodeLocation(lastLocation, completionHandler: { (placemarks, error) in
                if error == nil {
                    let firstLocation = placemarks?[0]
                    completionHandler(firstLocation)
                }
                else {
                    // An error occurred during geocoding.
                    completionHandler(nil)
                }
            })
        }
        else {
            // No location was available.
            completionHandler(nil)
        }
    }
}

