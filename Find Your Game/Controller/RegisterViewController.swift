//
//  RegisterViewController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 10/17/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit
import Firebase
import PopupDialog
import M13Checkbox

class RegisterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    @IBOutlet var RegisterView: UIView!
    let db = Firestore.firestore()
    var ages = Array(10...100)
    var agePicker = UIPickerView()
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var emailAddress: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var age: UITextField!
    @IBOutlet weak var zipcode: UITextField!
    @IBOutlet weak var tcCheckBox: M13Checkbox!
    @IBOutlet weak var tcLabel: UILabel!
    
    @IBAction func tcCheckBoxPressed(_ sender: Any) {
        view.endEditing(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //Setup age picker
        agePicker.delegate = self
        agePicker.dataSource = self
        age.inputView = agePicker
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        self.RegisterView.addGestureRecognizer(gesture)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = false
        let logo = UIImage(named: "gmg-banner")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.topItem?.hidesBackButton = false
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(ages[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        age.text = String(ages[row])
        self.view.endEditing(false)
    }
    
    func registerError(message: String) {
        let title = "Error"
        let errorMessage = message
        let popup = PopupDialog(title: title, message: errorMessage)
        let OK = CancelButton(title: "OK") {}
        popup.addButtons([OK])
        self.present(popup, animated: true, completion: nil)
    }
    
    func missingFieldsError() {
        let title = "Error"
        let errorMessage = "Please fill out all fields"
        let popup = PopupDialog(title: title, message: errorMessage)
        let OK = CancelButton(title: "OK") {}
        popup.addButtons([OK])
        self.present(popup, animated: true, completion: nil)
    }
    
    @IBAction func registerPress(_ sender: Any) {
        if((self.emailAddress.text != "") && (self.password.text != "") && (self.age.text != "") && (self.username.text != "") && (self.zipcode.text != "")){
            Auth.auth().createUser(withEmail: emailAddress.text!, password: password.text!) { (user, error) in
                if error != nil {
                    self.registerError(message: (error?.localizedDescription)!)
                } else {
                    //success
                    print("Registration Successful!")
                
                    Auth.auth().signIn(withEmail: self.emailAddress.text!, password: self.password.text!) { (user, error) in
                        
                        if error != nil {
    //                        print(error)
                        } else {
                            print("Log in successful!")
                            
                            self.db.collection("users").document((Auth.auth().currentUser?.uid)!).setData([
                                "Username": self.username.text,
                                "Email": self.emailAddress.text,
                                "Age": self.age.text,
                                "Zipcode": self.zipcode.text,
                            ]) { err in
                                if let err = err {
                                    print("Error adding document: \(err)")
                                } else {
                                    print("Document added with ID:")
                                }
                            }
                            
                            
                            }
                            
                        }
                    }
                    
                    self.performSegue(withIdentifier: "registerLogin", sender: (Any).self)
                }
            }
            else {
                //Pop modal to tell user to fill out all fields
                self.missingFieldsError()
            }
        }
    

}


