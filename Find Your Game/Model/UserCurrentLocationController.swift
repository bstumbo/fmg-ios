//
//  UserCurrentLocationController.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 9/16/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import UIKit

class UserCurrentLocationController: UITabBarController {
    var currentUser = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func shouldPerformSegue(withIdentifier identifier:String, sender: Any?) -> Bool {
        return !(identifier == "login")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "login" {
    
        }
    }
    
    
    
}
