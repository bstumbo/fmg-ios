//
//  User.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 9/16/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import Foundation
import CoreLocation

struct User: Codable {
    var userLat : CLLocationDegrees = 0
    var userLong : CLLocationDegrees = 0
//    @objc dynamic var userLocation : CLLocationCoordinate2D? = nil
    var userCurrentAddress : String = ""
    var uid : String = ""
    var email : String = ""
    var age : Int = 0
    var name : String = ""
    var zipcode : String = ""
}
