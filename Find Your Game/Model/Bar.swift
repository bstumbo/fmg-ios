//
//  Bar.swift
//  Get My Game
//
//  Created by Brandon Stumbo on 6/3/18.
//  Copyright © 2018 Get My Game, LLC. All rights reserved.
//

import Foundation
import CoreLocation

class Bar {
    @objc dynamic var title : String = ""
    @objc dynamic var nid : String = ""
    @objc dynamic var lat : CLLocationDegrees = 0
    @objc dynamic var long : CLLocationDegrees = 0
    @objc dynamic var distance : CLLocationDistance = 0
    @objc dynamic var location : String = ""
    @objc dynamic var description : String = ""
    @objc dynamic var imageURL : String = ""
    @objc dynamic var hours : String = ""
    @objc dynamic var phone : String = ""
    @objc dynamic var teams : String = ""
    @objc dynamic var leagues : String = ""
    @objc dynamic var sports : String = ""
    @objc dynamic var whatWatch : Array<Any> = []
    @objc dynamic var rating: Double = 0.00
    @objc dynamic var website: String = ""
}


